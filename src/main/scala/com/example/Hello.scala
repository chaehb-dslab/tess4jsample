package com.example

import java.io.File

import net.sourceforge.tess4j.Tesseract

object Hello {
  def main(args: Array[String]): Unit = {
    println("Hello, Tess4j!")

    test1
  }

  def test1 = {
//    System.setProperty("jna.library.path"," /usr/local/lib")
    val image:File = new File("samples/korean/post.jpg")

    val instance = new Tesseract
    instance.setLanguage("kor")

    val result = instance.doOCR(image)

    println("Result : "+result)
  }
}
