name := """Tess4JSample"""

version := "1.0"

scalaVersion := "2.11.7"

// Change this to another test framework if you prefer
libraryDependencies ++= Seq(
  "net.sourceforge.tess4j" % "tess4j" % "2.0.1",
  "org.seleniumhq.selenium" % "selenium-java" % "2.48.2",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

